# Handbuch JOY-IT USB PD Trigger Modul

It is an alternative german handbook about the USB PowerDelivery Trigger Modul from JoyIT. [reichelt.de DEBO USB-PD 1](https://www.reichelt.de/entwicklerboards-usb-pd-trigger-modul-usb-c-auf-klemme-debo-usb-pd-1-p318779.html)

## License
[CC Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/)
