## JOY-IT USB PD Trigger Modul

An USB Anschlüssen mit PowerDelivery (PD) kann die Platine mit dem Chipsatz der Stromquelle kommunizieren und ein Hochschalten der Spannung erfragen. Das funktioniert nur, wenn vollständig USB-C genutzt wird und das Kabel geeignet ist. Eine Stromquelle braucht nur einige Modi unterstützen. Ein Schnellladeanschluss mit USB-A macht vermutlich ein anderes Protokoll.

Wenn man die Platine einfach anschließt versucht die Platine die zuletzt gespeicherte Spannung auszuhandeln. Schlägt die Aushandlung der Spannung fehl, blinkt die LED weiß und es liegen meistens 5V an.

Um eine Spannung als Standard zu speichern, hält man den Knopf auf der Platine gedrückt und schließt die Platine an das Netzteil an. Die LED blinkt bunt, bis man den Knopf loslässt. Durch wiederholtes Drücken des Knopfes kann man die Spannung auswählen. Wenn die gewünschte Spannung erreicht ist, hält man den Knopf gedrückt, um die Auswahl zu bestätigen. Zuletzt trennt man die Spannungsversorgung vom Modul und verbindt sie anschließend wieder, damit das Modul nun direkt in diesem Modus startet. Manche Powerbänke registrieren den geringen Stromverbrauch der Platine nicht und schalten nach einiger Zeit wieder ab.

Die Platine unterstützt max 5A, 20V => 100W. Auf ausreichenden Kabelquerschnitt achten! Maße (LxBxH): 31,5 x 15 x 14 mm

Das Hochschalten nach dem Anschließen hat mit der Grixx Powerbank von Action nicht funktioniert. Es wird nur die gespeicherte Spannung abgegeben, wenn das USB-C Kabel in die ausgeschaltete Powerbank gesteckt wird. An- und Ausschalten mit Hilfe des Knopfes der Powerbank funktioniert nicht. Auf der Grixx Powerbank muss das Blitz Symbol gezeigt werden, dann ist der Power Delivery Modus aktiv.


### Tabelle: LED Farbe - Spannung

Moduswechsel durch Tastendruck

*LEDFARBE*     *SPANNUNG*  
Rot              5V  
Gelb             9V  
Grün            12V  
Hellblau        15V  
Dunkelblau      20V  
Lila            Höchst mögliche Spannung  
(blinkt)        Läuft durch alle Modi (pro Modus 1 Sekunde)  
blinkt Weiss	Fehler, angeforderte Spannung konnte nicht ausgehandelt werden, vermutlich liegen 5V an.  


### Schematische Zeichnung

JoyIT USB PD Trigger Modul

~~~
         ___
     .--(___)--.  Eingang
     |  USB-C  |
     |         |
     |   LED []|  Statusanzeige
     |         |
     |  Knopf 0|  Bedienung
     |         |
     | Schraub-|
     | klemmen |
     | (+) (-) |  Ausgang
     `---------'
~~~
Bezugsquelle: reichelt.de DEBO USB-PD 1
